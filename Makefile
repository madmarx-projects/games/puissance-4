CC=gcc
LD=-L /usr/bin/ld
EXEC=puissance4

puissance4: puissance4.o fonctions.o fonctions.h
	$(CC) $^ -o $@

.PHONY: clean mrproper

clean:
	@rm -rf *.o

mrproper: clean
	@rm -rf $(EXEC)
