/*
  ╔═══════════════════════════════╗
  ║  Auteur:  Valentin MARX       ║
  ║  Groupe:  B                   ║
  ║  Sujet:   C                   ║
  ║  Date:    17/03/15            ║
  ║  Nom:     fonctions.c         ║
  ╚═══════════════════════════════╝
*/

#include "fonctions.h"

void messageAcceuil() {
  puts("\f\n\t\t  ╔═════════════╗");
  puts(    "\t\t  ║ Puissance 4 ║");
  puts(    "\t\t  ╚═════════════╝\n\n");

  puts("  Essayez d'aligner 4 pions de votre couleur dans la grille !\n");
  puts("                OK?");
  getchar();
}

void remplissage(int tab[][7]) {
  int i, j;
  for (i=0; i<6; ++i) {
    for (j=0; j<7; ++j) {
      tab[i][j] = -1;
    }
  }
}

void placementJeton(int *lig, int *col, int tab[][7], int player) {
  printf("\nJoueur ");

  if (player == 0) {
    couleur("31");
    printf("ROUGE");
    couleur("0");
  } else if (player == 1) {
    couleur("33");
    printf("JAUNE");
    couleur("0");
  }

  printf(", où voulez-vous placer votre pion? \n");

  int numCol;
  scanf("%d", &numCol);
  //vider_stdin();
  fflush(stdin);
  while ((numCol < 1 | numCol > 7) || tab[0][numCol-1] != -1) {
    if (tab[0][numCol-1] != -1) {
      puts("\nAttention, colonne déjà remplie !");
    }
    printf("Joueur ");
    if (player == 0) {
      couleur("31");
      printf("ROUGE");
      couleur("0");
    } else if (player == 1) {
      couleur("33");
      printf("JAUNE");
      couleur("0");
    }
    printf(", veuillez indiquer un numéro de colonne correct:  ");
    scanf("%d", &numCol);
    //vider_stdin();
    fflush(stdin);
  }

  numCol--;    //pour transformer en coordonnées exploitables

  int i = 5;    //ligne
  while (tab[i][numCol] != -1) {
    i--;
  }

  tab[i][numCol] = player;
  *col = numCol;
  *lig = i;
}

int verifGagnant(int lig, int col, int grille[][7], int joueur) {
  int gagnant = verifVerticale(col, grille, joueur);

  if (gagnant == 0) {
    gagnant = verifHorizontale(lig, grille, joueur);
  }
  if (gagnant == 0) {
    gagnant = verifObliqueCroissante(lig, col, grille, joueur);
  }
  if (gagnant == 0) {
    gagnant = verifObliqueDecroissante(lig, col, grille, joueur);
  }

  return gagnant;
}

int verifVerticale(int j, int tab[][7], int player) {

  int i = 5, won = 0, cpt = 0;
  while(i>=0 && won==0) {
    if (tab[i][j] == player) {
      cpt++;
    } else {
      cpt = 0;
    }

    if (cpt == 4) {
      won = 1;
    }

    i--;
  }

  return won;
}

int verifHorizontale(int i, int tab[][7], int player) {
  int j = 0;

  int won = 0, cpt = 0;

  while(j<7 && won==0) {
    if (tab[i][j] == player) {
      cpt++;
    } else {
      cpt = 0;
    }

    if (cpt == 4) {
      won = 1;
    }

    j++;
  }

  return won;
}

int verifObliqueCroissante(int i, int j, int tab[][7], int player) {
  while(i<5 & j>0) {
    i++;
    j--;
  }

  int won = 0, cpt = 0;

  while((i>=0 & j<7) && won==0) {
    if (tab[i][j] == player) {
      cpt++;
    } else {
      cpt = 0;
    }

    if (cpt == 4) {
      won = 1;
    }

    i--;
    j++;
  }

  return won;
}

int verifObliqueDecroissante(int i, int j, int tab[][7], int player) {
  while(i>=0 & j>6) {
    i--;
    j--;
  }

  int won = 0, cpt = 0;

  while ((i<6 & j<7) && won==0) {
    if (tab[i][j] == player) {
      cpt++;
    } else {
      cpt = 0;
    }

    if (cpt == 4) {
      won = 1;
    }

    i++;
    j++;
  }

  return won;
}

int changementJoueur(int turn) {
  int player = turn % 2;

  return player;
}

void affichageGrille(int tab[][7], int turn) {
  int i, j;
  system("clear");
  printf("\n\t┌───────────────┐\n");
  printf(  "\t│   Tour n° %d\t│\n", turn);
  printf(  "\t├───┬───┬───┬───┼───┬───┬───┐\n\t");
  printf(    "│ 1 │ 2 │ 3 │ 4 │ 5 │ 6 │ 7 │\n");
  for(i=0; i<6; ++i) {
    printf("\t├───┼───┼───┼───┼───┼───┼───┤\n\t");
    for(j=0; j<7; ++j) {
      printf("│ ");
      if (tab[i][j] == -1) {
        printf(" ");
      } else
        if (tab[i][j] == 0) {
          couleur("31");
          printf("O");
          couleur("0");
        } else
          if (tab[i][j] == 1) {
            couleur("33");
            printf("O");
            couleur("0");
          }
      printf(" ");
    }
    printf("│");
    printf("\n");
  }
  printf(    "\t└───┴───┴───┴───┴───┴───┴───┘\n");
}

void affichageGagnant(int player) {
  if (player == 0) {
    printf("\n\t   Le joueur ");
    couleur("31");
    printf("ROUGE");
    couleur("0");
    printf(" a gagné\n\n\n");
  } else if (player == 1) {
    printf("\n\t   Le joueur ");
    couleur("33");
    printf("JAUNE");
    couleur("0");
    printf(" a gagné\n\n\n");
  }
}

void vider_stdin(void) {
  scanf("%*[^\n]");
  getchar();
}

