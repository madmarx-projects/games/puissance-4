/*
  ╔═══════════════════════════════╗
  ║  Auteur:  Valentin MARX       ║
  ║  Groupe:  B                   ║
  ║  Sujet:   C                   ║
  ║  Date:    17/03/15            ║
  ║  Nom:     fonctions.h         ║
  ╚═══════════════════════════════╝
*/

#include <stdio.h>
#include <stdlib.h>

#define couleur(param) printf("\033[%sm",param)

//---------------------------------------------//

void messageAcceuil();

/*
 * Pré:   tab[6][7] est initialisé
 * Post:  tab[6][7] contient des -1 dans chacune de ses cases
 */
void remplissage(int tab[][7]);

/*
 * Pré:   tab[6][7] est initialisé
 *        lig et col sont initialisés
 *        player est initialisé et vaut 0 pour le joueur rouge ou 1 pour le joueur jaune
 * Post:  col et lig renseignent les coordonnées du dernier pion placé
 *        tab est modifié, le pion est placé selon le choix du joueur
 */
void placementJeton(int *lig, int *col, int tab[][7], int player);

/*
 * Pré:  grille[6][7] est initialisé
 *       lig et col sont initialisés
 *       player est initialisé et vaut 0 pour le joueur rouge ou 1 pour le joueur jaune
 * Post:  lig, col, grille et player sont inchangés
 * Rés:   renvoie 0 si pas de gagnant, renvoie 1 si il y a un gagnant
 */
int verifGagnant(int lig, int col, int grille[][7], int joueur);

/*
 * Pré: j est initialisé et correspond à la colonne dans laquelle le dernier pion a été placé
 *      tab est initialisé
 *      player est initialisé et vaut 0 pour le joueur rouge ou 1 pour le joueur jaune
 * Post:  j, tab et player sont inchangés
 * Rés:    gagnant == 0 si pas de gagnant trouvé sur la colonne
       gagnant == 1 si un gagnant est trouvé sur la colonne
 */
int verifVerticale(int j, int tab[][7], int player);

/*
 * Pré:   i est initialisé et correspond à la ligne dans laquelle le dernier pion a été placé
 *        tab est initialisé
 *        player est initialisé et vaut 0 pour le joueur rouge ou 1 pour le joueur jaune
 * Post:  i, tab et player sont inchangés
 * Rés:   gagnant == 0 si pas de gagnant trouvé sur la ligne
 *        gagnant == 1 si un gagnant est trouvé sur la ligne
 */
int verifHorizontale(int i, int tab[][7], int player);

/*
 * Pré:   i et j sont initialisés et correspondent à la ligne et à la colonne dans lesquelles le dernier pion a été placé
 *        tab est initialisé
 *        player est initialisé et vaut 0 pour le joueur rouge ou 1 pour le joueur jaune
 * Post:  j, tab et player sont inchangés
 * Rés:   gagnant == 0 si pas de gagnant trouvé sur la diagonale croissante
 *        gagnant == 1 si un gagnant est trouvé sur la diagonale croissante
 */
int verifObliqueCroissante(int i, int j, int tab[][7], int player);

/*
 * Pré:   i et j sont initialisés et correspondent à la ligne et à la colonne dans lesquelles le dernier pion a été placé
 *        tab est initialisé
 *        player est initialisé et vaut 0 pour le joueur rouge ou 1 pour le joueur jaune
 * Post:  i, j, tab et player sont inchangés
 * Rés:   gagnant == 0 si pas de gagnant trouvé sur la diagonale décroissante
 *        gagnant == 1 si un gagnant est trouvé sur la diagonale décroissante
 */
int verifObliqueDecroissante(int i, int j, int tab[][7], int player);

/*
 * Pré:   turn correspond au numero du tour en cours
 * Post:  turn est inchangé
 * Rés:   inversion du player:
 *        0 si player vaut 1
 *        1 si player vaut 0
 */
int changementJoueur(int turn);

/*
 * Pré:   tab est initialisé
 *        turn correspond au numero du tour en cours
 * Post:  tab et turn sont inchangés
 */
void affichageGrille(int tab[][7], int turn);

/*
 * Pré:   joueur est initialisé
 * Post:  joueur est inchangé
 */
void affichageGagnant(int joueur);

void vider_stdin(void);
