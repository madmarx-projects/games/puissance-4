# Puissance 4

Puissance 4 réalisé dans le cadre du cours de C en 2014.

## Illustration

![screenshot](https://i.imgur.com/gTWpihi.png "puissance4 screenshot")

## Compile

``` bash
make
```

## Run

``` bash
./puissance4
```

