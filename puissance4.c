/*
  ╔═══════════════════════════════╗
  ║  Auteur:  Valentin MARX       ║
  ║  Groupe:  B                   ║
  ║  Sujet:   C                   ║
  ║  Date:    17/03/15            ║
  ║  Titre:   puissance4.c        ║
  ╚═══════════════════════════════╝

  Consignes avant lancement de l'application:
    --> Requiert Linux absolument
    --> Est fortement conseillé de mettre l'encodage de la console
      sur UTF-8 car présence d'accents et autres caractères spéciaux
      et semi-graphiques.
*/

#include "fonctions.h"

int main(int argc, char const *argv[]) {

  messageAcceuil();

  int grille[6][7];
  remplissage(grille);

  int joueur = 0, lig = 0, col = 0, tour = 1, gagnant = 0;

  affichageGrille(grille, tour);
  while (gagnant == 0 && tour <= 42) {

    joueur = changementJoueur(tour);

    placementJeton(&lig, &col, grille, joueur);
    affichageGrille(grille, tour);

    gagnant = verifGagnant(lig, col, grille, joueur);

    tour++;
  }

  if (gagnant == 1) {
    affichageGagnant(joueur);
  } else {
    printf("\n\t\tPas de gagnant !\n\n\n");
  }

  return 0;
}
